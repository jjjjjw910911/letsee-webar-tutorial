
## 소개

Letsee WebAR SDK는 모바일 AR 애플리케이션을 웹에서 쉽고 빠르게 구현할 수 있도록 해 줍니다. WebGL 기반의 3D 콘텐츠와 작동 가능한 HTML 요소를 이용하여 사물, 환경과 현실감 있게 인터랙션 하는 AR 경험을 편리하게 만들 수 있습니다.

## 요구사항

Letsee WebAR SDK가 실행되려면 다음과 같은 기능이 지원되는 모바일 브라우저가 필요합니다.

- WebGL (canvas.getContext(‘webgl’) || canvas.getContext(‘webgl2’))
- getUserMedia (navigator.mediaDevices.getUserMedia)
- deviceorientation (window.DeviceOrientationEvent)
- Web-Assembly/WASM (window.WebAssembly)

주의사항: Letsee WebAR SDK의 실행은 `https`를 통해서만 가능합니다. 이것은 브라우저가 카메라에 접근하기 위해 필요합니다.  
위 내용들은 다음과 같은 iOS와 Android 장치에 적용됩니다.

- iOS:
  - OS Version: iOS 11 이상
  - 지원 브라우저:
    - Safari
      - 주의사항: Safari에서 getUserMedia의 호출은 UIWebView나 WKWebView가 아닌 비디오 입력 장치를 직접 반환합니다. 
- Android:
  - 지원 브라우저:
    - Chrome
    - Chrome-variants (e.g. Samsung Browser)
    - Firefox

## 샘플 프로젝트 다운로드

샘플 프로젝트는 [https://github.com/letsee/tutorials](https://github.com/letsee/tutorials)의 Letsee 공개 GitHub 저장소에서 찾을 수 있습니다. 클론 또는 다운로드 하십시오.

## 인증키
개발 및 배포를 위해서는 인증키가 필요합니다. 현재 Letsee는 개발자들을 위한 체험용 키를 발급하고 있습니다. [SDK 사용을 위한 키는 이곳에서 신청이 가능합니다.](https://www.letsee.io/ko/download-sdk/) 


?> 인증키 및 SDK에 대한 추가 문의는 [contact@letsee.io](mailto:contact@letsee.io)로 연락 주시기 바랍니다.


## 웹 프로젝트에 앱 키 추가
앱키를 사용하여 Web AR SDK를 로드합니다. 로드 후에는 간단한 설정으로 앱을 시작할 수 있습니다.
프로젝트에 앱 키를 추가하십시오. Letsee의 샘플 프로젝트를 사용하는 경우 index.html을 편집하고 아래 코드 블럭의 *your_app_key*를 복사한 실제 앱 키로 변경합니다.
### SDK 로딩
[렛시 개발자 사이트](https://developer.letsee.io/)에서 발급받은 키를 *YOUR_APP_KEY*부분에 입력하면 됩니다.
```html
<script src="https://app.letsee.io/webar?key=YOUR_APP_KEY"></script>
```
### Web AR 어플리케이션 시작
```js
const config = {
  type: "IMAGE" | "MARKER"
};
```

## 애플리케이션 배포

### 웹 서버에 배포하기
Letsee WebAR SDK를 이용한 증강현실 애플리케이션의 배포는 일반적인 웹 서버를 통한 웹 앱의 배포와 방법이 같습니다. 개발용 웹 서버가 있는 경우, 샘플 프로젝트를 업로드하고 모바일 장치에서 접속하십시오. 브라우저로 모바일 장치의 카메라에 액세스하려면 HTTPS로 연결해야 합니다.

### 로컬 서버 설정하기
대부분의 경우 개인용 컴퓨터에서 개발하고 모바일 장치에서 확인하기 때문에 개발자에게는 로컬 웹 서버의 구축을 권장합니다. 로컬 웹 서버의 설정은 일반적으로 다양한 방법으로 수행할 수 있으나 이 문서에서는 Python 또는 Node.js를 이용한 두 가지 방법을 설명합니다. 설치된 로컬 웹 서버에 HTTPS 연결을 쉽게 설정할 수 있는 방법도 소개합니다.

Python의 경우 2.x와 3.x 버전에 따라 모듈의 이름이 다릅니다. 설치된 Python 버전을 확인하고 그에 맞는 명령어를 실행하십시오. 

```bash
// Python 버전 확인
$ python -V

// Python 2.x
$ python -m SimpleHTTPServer 8080

// Python 3.x
$ python -m http.server 8080
```


이 모듈은 기본적으로 명령이 실행된 경로를 루트로 설정하고 루트로 지정된 폴더를 웹서버로 동작시킵니다. 위 명령어는 8080포트를 이용하여 http서버를 동작시킨 예제입니다.  

자세한 옵션은 다음 링크를 참고하십시오. ([SimpleHTTPServer - Python 2.x](https://docs.python.org/2/library/simplehttpserver.html), [http.server - Python 3](https://docs.python.org/3/library/http.server.html))



Node.js를 이용한 로컬 웹 서버 실행을 위해서는 Node.js와 npm이 설치되어 있어야 합니다. 설치 방법은 [여기](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)를 참고하십시오. 웹 서버 패키지의 설치와 실행을 위한 명령어는 다음과 같습니다.

```bash
$ npm install http-server -g
$ http-server
```

이 패키지는 기본적으로 ./public 폴더 또는 실행된 지점인 ./ 폴더를 루트로 설정하고 8080 포트에 바인딩 합니다. 

자세한 옵션은 [여기](https://www.npmjs.com/package/http-server)를 참고하십시오.

브라우저로 모바일 장치의 카메라에 액세스하려면 HTTPS로 연결해야 합니다. 그러나 개발용 로컬 웹 서버 구성에서 이 문제를 가장 간편하게 해결하는 방법은 ngrok의 무료 플랜을 이용하는 것입니다. 아래 명령은 8080번 포트의 로컬 웹서버를 외부에서 접속할 수 있는 ngrok의 터널링 기능을 실행합니다. 자세한 옵션은 [여기](https://ngrok.com/docs)를 참고하십시오.

```bash
$ ngrok http 8080
```

정상적으로 8080 포트가 연결되면 아래와 같은 화면이 표시되고 ngrok에서 제공하는 url로 접속이 가능합니다.

![ngrock status window](assets/ngrok_screen.png)

## 모바일 접속 시 HTTPS 인증

브라우저를 통해 모바일 카메라에 엑세스 하기 위해서는 HTTPS 인증서가 필요하기 때문에 로컬호스트 환경에서 웹앱을 제공하는것은 불편할 수 있습니다. 튜토리얼에서는 편의상 위에서 언급한 Python이나 nodejs를 이용하여 로컬 https 웹 서버를 실행하는 방식으로 예제소스에 접속할 수 있습니다.

로컬 HTTPS서버를 구축하고 모바일 브라우저에서 테스트할때, **연결이 비공개로 설정되어 있지 않습니다.** 라는 문구의 화면이 뜰 경우가 있습니다. 그럴때는 아래와 같이 해결하십시오.
<br/>

### Android

1. 브라우저에서 인증서가 유용하지 않다는 창이 나올시 **고급** 버튼을 클릭합니다.
<br/><br/><br/>
<center><img src="assets/https-connection-1.png" width="360" height="710" style="border:1px solid gray"/></center>
<br/><br/>

2. **안전하지 않음으로 이동** 부분을 클릭하여 웹앱으로 접속합니다.
<br/><br/><br/>
<center><img src="assets/https-connection-2.png" width="360" height="710" style="border:1px solid gray"/></center>


### iOS

1. 브라우저에서 인증서가 유용하지 않다는 창이 나올시 **고급** 버튼을 클릭합니다.
<br/><br/><br/>
<center><img src="assets/https-connection-ios-1.png" width="360" height="710" style="border:1px solid gray"/></center>
<br/><br/>

2. **안전하지 않음으로 이동** 부분을 클릭하여 웹앱으로 접속합니다.
<br/><br/><br/>
<center><img src="assets/https-connection-ios-2.png" width="360" height="710" style="border:1px solid gray"/></center>

<!--
브라우저를 통해 모바일 카메라에 엑세스 하기 위해서는 HTTPS 인증서가 필요하기 때문에 로컬호스트 환경에서 웹앱을 제공하는것은 불편할 수 있습니다. 튜토리얼에서는 편의상 위에서 언급한 Python이나 nodejs를 이용하여 로컬 https 웹 서버를 실행하는 방식으로 예제소스에 접속할 수 있습니다.  
로컬 HTTPS서버를 구축하고 모바일 브라우저에서 테스트할때, **연결이 비공개로 설정되어 있지 않습니다.** 라는 문구의 화면이 뜰 경우가 있습니다. 그럴때는 아래와 같이 해결하십시오.
<br>
1. 브라우저에서 인증서가 유용하지 않다는 창이 나올시 **고급** 버튼을 클릭합니다.
<br>  
![https 접속 에러](assets/https-connection-1.png ':size=300')

<br><br>
2. **안전하지 않음으로 이동** 부분을 클릭하여 웹앱으로 접속합니다.
<br>  
![https 접속 에러](assets/https-connection-2.png ':size=300')
-->

