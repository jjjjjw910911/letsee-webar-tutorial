이 섹션에서는 다채로운 WebAR 애플리케이션을 구현할 수 있도록 SDK의 설정 방법과 API를 자세히 설명합니다. 

## 설정하기
웹 페이지에 AR 경험을 추가하려면 우선 Letsee WebAR SDK를 포함시키고, appKey와 trackerType을 설정해야 합니다.

|키|필요조건|설명|
|:---|:---|:---|
|appKey|필수|라이센스를 확인하는 인증 키를 기입합니다.|
|trackType|필수|대상 인식 모드를 설정합니다.<br/>**IMAGE, MARKER** 중 하나를 선택할 수 있습니다.<br/>Image Tracker는 가장 먼저 선언된 -letsee-target의 entity 1개를 인식하고 추적합니다.<br/>Marker Tracker는 지정된 5개의 동시 추적이 가능합니다.|

엔진이 활성화되면 브라우저의 viewport가 카메라를 배경으로 한 AR의 UI에 대응하도록 viewport가 강제로 지정됩니다.

## 인식대상 관리

### 엔터티

엔터티는 현실세계의 요소를 증강현실 웹 앱에 담기 위해 Letsee가 제안하는 추상화 개념입니다. 엔터티는 사물, 장소, 사람 등 현실에 존재하는 물리 객체로서, Letsee WebAR SDK에 현실세계 요소를 특정하여 인식의 대상으로 지정하는 데 사용됩니다. 이것은 형상, 크기, 특징, 위치 등으로 이루어진 공간적(spatial) 속성을 가집니다. 
모든 엔터티는 HTTPS로 접근 가능한 웹 리소스입니다. 따라서 Letsee WebAR SDK는 ARWebApp 코드 안에서 선언된 URI(Uniform Resource Identifier)를 통해 물리적으로 식별할 수 있는 데이터를 받아오고 추적하여 웹앱의 렌더링에 반영합니다. 또한 필요한 경우 엔터티를 직접 선언하여 자신의 웹앱에 추가할 수 있습니다.

|속성|필요조건|설명|
|:---|:---|:---|
|uri|필수|엔터티 고유의 식별자입니다. 파일명과 같은 이름을 입력합니다.|
|name|선택|오브젝트의 이름을 입력합니다.|
|size|필수|오프젝트의 가로x세로 사이즈를 입력합니다. 단위는 mm입니다.<br/><ul><li>Image Tracker : 이미지나 사물의 실측 사이즈를 입력해주시면 됩니다.</li><li>Marker Tracker: 사용하시고자 하는 마커의 실측 사이즈를 입력해주시면 됩니다.</li></ul>|
|scale|선택|증강되는 컨텐츠의 스케일을 입력합니다. 1의 경우 위에 입력한 size와 1:1 매칭되는 증강 컨텐츠를 렌더링합니다.|
|image|선택|Image 트래커에 사용될 레퍼런스 이미지 입니다. http나 https로 시작되는 url이나 상대 경로를 입력할 수 있습니다.<br/> 단, 상대 경로를 입력할 경우 AR App이 구동되는 웹페이지의 경로에 주의해 주시기 바랍니다.</br><ul><li>확장자: jpg, png</li><li>용량: 최대 2MB</li><li>픽셀 사이즈: 가로와 세로 픽셀의 곱이 640,000px 이하</li></ul> |
|letseeMarkerId|선택|Marker 트래커에 사용될 마커 아이디 입니다.<br/> 0부터 249의 총 250개의 마커를 사용할 수 있으며 입력은, “marker_” + 숫자의 형식으로 사용하면 됩니다. <br/>  **마커 ID 숫자 표기는 아래를 참고하세요.**  <br/>  **( 올바른 사용예시 : marker_1, marker_2 .... )**<br/>**( 잘못된 사용예시 : marker_<span style="color:red">00</span>1, marker_<span style="color:red">00</span>2 ....)**|

엔터티 정보는 JSON(JavaScript Object Notation) 포맷으로 표현됩니다. 

```json
{
    "uri":"letsee-marker.json",
    "name":"Sample entity",
    "size":{
        "width": 200, // 마커이미지의 실제 출력된 사이즈를 mm 단위로 입력해 주세요.
        "height": 140 
    },
    "scale": 1,
    "image":"image.jpg"
}
```

위와 같이 정의된 엔터티는 이어서 작성할 AR WebApp 코드 내에 선언하여 사용될 수 있으며, Letsee Engine이 시작할 때 데이터를 로드하여 참조합니다. Letsee 엔진에 설정된 인식 방법(image, marker 등)에 부합하는 descriptor가 엔터티에 선언되어 있으면 이를 바탕으로 카메라 영상 내에서 엔터티를 식별하고 추적합니다.


## 이미지 타겟 (Image Targets)

Letsee의 WebAR SDK는 2D 이미지 파일에 대한 감지(Detection) 및 트래킹(Tracking)을 제공하며 이를 이용하여 해당 이미지파일 위에 컨텐츠(HTML overlay)를 AR 컨텐츠로 배치할 수 있습니다. 
이미지 대상은 인쇄 매체, 포스터 또는 제품 포장과 같은 평평한 이미지입니다.

<br/><br/>
- - -
<br/><br/>

### 이미지 타겟의 요구조건 (Requirements)

* 파일 형식: 8비트 또는 24비트 형식의 **.jpg**  **.png**
* 용량: 최대 **2MB**
* 픽셀 사이즈: 가로픽셀과 세로픽셀의 곱이 **640,000** pixels 이하

|GOOD pixel size|BAD pixel size|
|:---------: | :---------: |
|320 X 240 => <span style="color:blue">76,800 pixel</span>|1024 X 768 => <span style="color:red">786,432 pixel</span>|
|640 X 480 => <span style="color:blue">307,200 pixel</span>|1280 X 800 => <span style="color:red">1,024,000 pixel</span>|
|800 X 600 => <span style="color:blue">480,000 pixel</span>|1680 X 1440 => <span style="color:red">2,419,200 pixel</span>|
|1000 X 600 => <span style="color:blue">600,000 pixel</span> |1920 X 1200 => <span style="color:red">2,304,000 pixel</span>|
|**픽셀 사이즈 =< 640,000**|**픽셀 사이즈 => 640,000**|
|가로,세로의 픽셀 사이즈의 곱이 640,000 pixels<br/> 미만이므로 적합한 이미지|가로,세로의 픽셀 사이즈의 곱이 640,000 pixels<br/> 초과하므로 적합하지 않은 이미지|

이미지 타겟으로 영화 포스터나 제품 포장과 같은 평면 이미지를 이용하여 프로젝트 패키지(디렉토리)에 저장한 뒤, 해당 이미지 타겟을 추적(tracking)하는 Letsee 웹 애플리케이션의 빌드를 시작할 수 있습니다.

다음 기사는 이미지 타겟으로 적합한 이미지를 선택하고 해당 이미지를 디자인하는 방법에 대한 지침을 제공합니다.

<br/><br/>
- - -
<br/><br/>

## 이미지 타겟 최적화 

최상의 이미지 대상 추적 경험을 보장하려면 이미지 타겟을 선정 할 때 다음 지침을 따르십시오.

**권장 사항:**
* 특징점과 디테일이 많은 이미지
* 고대비 이미지

**유의 사항:**
* 반복적인 패턴이 들어간 이미지
* 너무 밝은 조명을 비추거나, 불분명한 이미지
* 저해상도 이미지 (광택 또는 흐림)

**색상:** 이미지 대상의 색상은 추적 및 감지에 큰 영향을 미치지 않습니다. 최상의 결과를 얻으려면 이미지 타겟 추적에 평평한 표면을 사용하십시오.
그러나 이미지 대상의 실제 물리적인 반사성을 고려하십시오. 광택있는 표면 및 화면 반사로 트래킹에 대한 성능을 저하시킬 수 있습니다.
최적의 추적 성능을 위해 확산 조명 조건에서 이미지 타겟을 이용하십시오.

요약하면 Image Target의 다음과 같은 속성 및 기능을 통해 AR 앱에 대한 최상의 감지(Detection) 및 추적(Tracking) 성능을 얻을 수 있습니다.


<br/><br/>
예시:

|좋은 이미지 예시|좋지 않은 이미지 예시|
|:---------: |:---------: |
|![house-high-contrast](assets/good-example.jpg ':size=600') | ![house-low-contrast](assets/bad-example.jpg ':size=600')|
|대비가 좋고 구체적인 이미지|대비가 좋지 않고 구체적이지 않은 이미지|

<br/>

|좋은 이미지 예시|좋지 않은 이미지 예시|
|:---------:|:---------:|
|![street-scene-good](assets/good-example-02.jpg ':size=600') |![street-scene-bad](assets/bad-example-02.jpg ':size=600')|
|세부적이고 분명한 이미지|세부적이지 않고 특징이 없는 이미지|

<br/><br/>

중요한 것은 추적에 대한 최상의 결과를 얻기 위해 반복적인 패턴이나 주제를 피하십시오.
다음 이미지에는 충분한 기능과 좋은 대비가 포함되어 있지만 반복적인 패턴은 감지 성능에 영향을 줍니다.<br>

최상의 결과를 얻으려면 반복되는 문양 (회전 또는 스케일 변화시의 문양도 포함) 또는 강한 회전 대칭이 없는 이미지를 선택하십시오.
이미지를 보다 쉽게 ​​구별 할 수 있도록 색상과 대비를 변경하여 텍스트, 아이콘 또는 이미지 사용자 정의와 같은 기능을 추가해야 할 수도 있습니다.

- - -
<br/><br/>

### 조명 환경 

테스트 환경의 조명 환경은 대상 감지 및 추적에 크게 영향을 줄 수 있습니다.

* 장면이나 특징점이 카메라의 뷰안에 잘 비춰지도록 실내 또는 작업 공간에 충분한 조명이 있는지 확인하십시오.
* 밝기가 너무 밝으면 결과가 좋지 않을 수 있습니다.
* Letsee WebAR SDK는 조명 조건이 일반적으로 더 안정적이고 제어하기 쉬운 실내 및 야외 환경에서 가장 잘 작동합니다.

<br/>

<!--
|Lighting conditions|
|:---------: |
|![brightness](assets/brightness.png)|
|작업 환경에 충분한 조명이 있는지 확인하십시오. 실내/실외의 경우, 안정적인 조명 환경이 조성되어 있는지 확인하십시오. <br/>(너무 어둡거나 밝지 않아야 함).|
-->

|너무 밝은 이미지 예시|너무 어두운 이미지 예시|너무 흐릿한 이미지 예시|
|:---------:|:---------:|:---------:|
|![too-right](./assets/bad-env-01.jpg ':size=600')|![too-dark](assets/bad-env-02.jpg ':size=600')|![too-blurry](assets/bad-env-03.jpg ':size=600')
|높은 수준의 밝기와 대비로 인해 이미지를 감지하기가 어렵습니다.|매우 낮은 조명은 이미지 검출 성능 저하로 이어질 수 있습니다.|과도한 조명과 흐릿한 이미지는 카메라 움직임 및 물체 감지에 영향을 줄 수 있습니다.|

<br/><br/>
- - -
<br/><br/>

## 마커 트래킹 (Marker Tracking)

이미지 타겟과 동일하게 Letsee WebAR SDK는 마커(단일 및 다중)를 감지하고 AR 애플리케이션을 빌드하기 위한 오버레이 컨텐츠를 작성하도록 도와줍니다.

[이곳에서 사용 가능한 모든 마커를 확인하고 필요에 따라 인쇄 할 수 있습니다.](https://github.com/letsee/tutorials/tree/master/resources/markers)

<br/><br/>
- - -
<br/><br/>

### 마커 트래킹 요구조건 (Requirement)

이미지 타겟과 달리 마커는 많은 구성이나 설정이 필요하지 않습니다.
[Markers list](https://github.com/letsee/tutorials/tree/master/resources/markers)에서 확인할 수 있듯이, 각 마커에는 자체`Id`가 있습니다.
해당 `Id`를 `json`파일에 등록하여 Entity(객체)를 설정할 수 있습니다.

<br/>

|Marker #0|Marker #1|Marker #2|Marker #3|Marker #4|
|:---:|:---:|:---:|:---:|:---:|
|![marker0](assets/marker_0.png)|![marker1](assets/marker_1.png)|![marker2](assets/marker_2.png)|![marker3](assets/marker_3.png)|![marker4](assets/marker_4.png)|

마커(Marker)가 모바일 디바이스의 카메라의 영역 안에 있다면, 어떠한 크기의 마커도 적용하여 사용할 수 있습니다.
그렇지만, 실제 마커의 크기는 반드시 `.json` file에 등록이 되어 있어야 합니다. 

|실측 사이즈 (mm)|
| :---------: |
|![actual-size](assets/marker-actual-size.png)|

<br/><br/>
- - -
<br/><br/>

## CSS로 HTML 요소 정합하기

### place 미디어 타입

`place` 미디어는 HTML 기반의 WebAR을 실현하기 위해 렛시가 제안하는 미디어 타입입니다. 미디어 타입은 HTML 문서를 렌더링 하기 위한 매체를 지정하기 위해 사용되며 대표적인 미디어 타입으로는 screen, print, speech 등이 있습니다. Letsee 엔진이 가동되면, Place 미디어로 선언된 CSS 구문들이 활성화 되어 HTML 문서를 다음과 같은 AR 모드로 렌더링 합니다.

* 브라우저의 배경화면이 카메라 영상으로 지정됩니다.

### 인식 대상 설정하기

렛시 WebAR SDK는 인식을 위한 대상 물체를 지정하기 위한 `-letsee-target` css 속성을 소개합니다. 이 속성은 object (uri)를 사용합니다. 여기서 uri는 브라우저가 대상 객체를 나타내는 엔터티 데이터를 로드하는 URL 문자열입니다. 예를 들어 다음 구문은 CSS 셀렉터로 선택된 HTML 요소와 주어진 URL로 선언된 엔터티를 연관시킵니다. 따라서 URI로 지정한 엔터티가 브라우저의 뷰에 들어가면 선택된 HTML 요소가 정합되어 보여집니다.

```css
selector {
  -letsee-target: uri('letsee-marker.json');
}
```

타겟으로 지정되지 않은 요소는 일반 HTML 요소가 화면에 표시되는 것처럼 렌더링됩니다. 타겟이 지정된 HTML 요소가 화면에 보이지 않더라도 여전히 DOM에는 있습니다. 비디오 또는 오디오 요소가 자동 재생으로 설정되어 있으면 엔터티 추적 여부에 관계없이 리소스가 로드되는 즉시 재생됩니다. 화면에 표시될 때에만 재생하거나 시간 의존성이 있는 경우, 대상 엔터티에 대한 트래킹이 시작 또는 중지될 때 이벤트 핸들러를 함께 설정해 주십시오.

### HTML 요소 배치하기

렛시 WebAR SDK는 물리 객체 주위에 가상 객체 (HTML 요소)를 3D로 배치하기 위해 `-letsee-transform` css 속성을 소개합니다. 이 속성은 `translate(x, y, z)` 함수를 사용하여 요소의 위치를 픽셀 단위로 지정합니다. 엔터티 설명에서 언급했듯이 엔터티는 평면 모양입니다. 엔터티의 좌표계는 다음과 같이 정의됩니다.

![이미지](assets/coords.png) 

y 축은 CSS 위치 속성의 방향과 반대입니다. 즉, 일반적인 CSS 스타일에서 일반적으로하는 것처럼 y 위치의 값을 높이는 대신 요소를 화면 아래로 (또는 대상 객체의 축 아래로) 이동하려면 값을 줄입니다.  
다음 예제를 보십시오.

```css
#hello {
  -letsee-target: uri('letsee-marker.json');
  -letsee-transform: translate(1, 2, 3);
}
```

위의 CSS는 id = hello인 HTML 요소를 x = 1, y = 2, z = 3에 배치합니다.

|속성|설명|
|:---|:---|
|-letsee-target|entity 정보가 담긴 json 파일의 경로를 입력합니다. http나 https로 시작되는 url이나 상대 경로를 입력할 수 있습니다.<br/> 단, 상대 경로를 입력할 경우 AR App이 구동되는 웹 페이지의 경로에 주의해 주시기 바랍니다.|
|-letsee-transform|인식된 화면상의 오브젝트의 중심점을 기준으로 어느 위치에 증강될 지를 결정합니다. 순서대로 x,y,z의 값을 px 기준으로 입력합니다.<br/>예를 들어 (-100, 100, 0)의 경우 오브젝트의 중심점을 기준으로 좌측으로 100px, 상단으로 100px 이동한 곳에 컨텐츠가 증강됩니다.<br/>css의 position: absolute와 유사하나, relative를 오브젝트의 센터에 두고 있다고 생각하시면 됩니다.|

```html
<head>
...
<style media="place" type="text/css">
#container {
    -letsee-target: uri('letsee-marker.json');
    -letsee-transform: translate(0, 0, 0);
}
</style>
...
</head>
```

## 이벤트

|이벤트|설명|
|:---|:---|
|onTrackStart|엔터티가 뷰에 들어가면 Letsee 엔진의 이벤트 관리자가 trackstart 유형의 이벤트를 전달합니다.|
|onTrackMove|엔터티가 뷰 안에서 이동하면 Letsee 엔진의 이벤트 관리자가 trackmove 타입의 이벤트를 전달합니다.<br/> trackmove 이벤트의 경우 이벤트 객체에는 장치 카메라를 기준으로 엔터티의 로컬 3D 변환 매트릭스가 포함됩니다.|
|onTrackEnd|엔터티가 뷰를 벗어나면 Letsee 엔진의 이벤트 관리자가 trackend 타입의 이벤트를 전달합니다. |
