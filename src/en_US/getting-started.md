# **Download a Sample Project**
Sample projects can be found on Letsee's public GitHub repo at [https://github.com/letsee/web-tutorial](https://github.com/letsee/web-tutorial).
Please clone or download .zip file to get started.

<br/><br/>
- - -
<br/><br/>

# **Trial App Key**
The Letsee WebAR SDK is currently only available as a trial. The trial version will be valid until **October 31, 2019**
without any functional limitations. Trial versions use one publicly available **AppKey**. Please copy and save this following
key for later use.

`your_app_key`

<br/><br/>
- - -
<br/><br/>

# **Add App Key to Your Project**
If you are using Letsee WebAR Trial version, copy the **trial AppKey** and replace the `your_app_key` in the `index.html` file.

```js
const config = {
    "appKey": "your_app_key",
    "trackerType": "IMAGE" | "MARKER"
};
```

<br/><br/>
- - -
<br/><br/>

# **Serve Application**
## **Deploy to A Web Server**
If you have a development web server, upload the sample project and connect from a mobile device.
You'll need to **connect via HTTPS** as browsers require `HTTPS` certificates to access the camera on your phone through a browser.

## **Deploy to A Local Server**
Developers are encouraged to build local web servers because in most cases they are developed on personal computers and verified on
mobile devices.

Setting up a local web server can usually be done in several way. This article describes two different methods using
Python (SimpleHTTPServer) or [Node.js](https://www.npmjs.com/package/http-server) as an easy way to set up an HTTPS
connection to an installed local web server.

#### Using Node.js
1. Install Node.js and npm:<br>
If you don't already have Node.js and npm installed, please get it here [https://www.npmjs.com/get-npm](https://www.npmjs.com/get-npm)
and for installation instruction, check it [here](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm).

2. Open Terminal and type following commands:
```bash
$ npm install http-server -g
$ cd <your_project_directory>
$ http-server
```

By default, this package sets the root of the `./public` folder (or the `./` folder) which is the point of execution and
binds it to port `8080`. For more options, please refer to [here](https://www.npmjs.com/package/http-server).

#### Using Python
Open Terminal and type following commands:

```bash
// Checking python version
$ python -v

$ cd <your_project_directory>

// Python 2.x
$ python -m SimpleHTTPServer

// Python 3.x
$ python -m http.server
```

This module defaults to the root of the command execution and binds to port 8000. See the following links for more options:
([SimpleHTTPServer-Python 2.x](https://docs.python.org/2/library/simplehttpserver.html), [http.server-Python 3](https://docs.python.org/3/library/http.server.html))

#### Using ngrok
Alternatively, you can use [ngrok](https://ngrok.com/download) to server application locally.
The following command executes the tunneling function of `ngrok`, which can access the local web server on port `8000` from the outside.

For detailed `ngrok` configuration, please check it [here]((https://ngrok.com/docs)).

```bash
$ ngrok http 8000
```

<br/><br/>
- - -
<br/><br/>
