
## **Image Targets**
Letsee WebAR SDK can detect and track 2D image files and create the overlay content as AR content onto it.
Image Targets are flat images, such as print media, posters or product packaging.

<br/><br/>
- - -
<br/><br/>

### **Image Target Requirements**

* File Types: Must be 8- or 24-bit **.jpg** or **.png**
* Capacity: Up to **2MB** in size.
* Pixel Size: The product of the horizontal and vertical pixels is **640,000** or less than.

You can use any planar image such as movie posters or product packaging, then put them into your project directory to start
tracking and building your application.

The following article will help you select appropriate target images and provide guidelines on how to design your target images.<br>
See: [Optimizing Target Detection and Tracking](/en_US/about-marker?id=_3optimizing-target-detection-and-tracking)

<br/><br/>
- - -
<br/><br/>

## **Markers Tracking**

Same as Image Target, Letsee WebAR SDK can detect markers (single and multiple) and help you to create overlay content to
build up your AR applications.

You can check all available markers in the [markerboard](assets/marker_board.png) and print out any of them for your need.

<br/><br/>
- - -
<br/><br/>

### **Marker Tracking Requirements**

In contrary to Image Target, Marker does not require too much configuration or settings.
From the [markerboard](assets/marker_board.png), each marker has its own `Id` which you can use for your `.json`
file to setup Entity.

|Marker #0|Marker #1|Marker #2|Marker #3|Marker #4|
|:---|:---|:---|:---|:---|
|![marker0](assets/marker_0.png)|![marker1](assets/marker_1.png)|![marker2](assets/marker_2.png)|![marker3](assets/marker_3.png)|![marker4](assets/marker_4.png)|

You can design **any size** for your markers as long as the markers are in the field of view of your mobile's camera
and the actual size of your marker MUST be declare correctly in the `.json` file.<br>

For more detail about how to setup marker, please check our [How to create WebAR application](/en_US/how-to-create-web-ar-applications.md)
before go more further in your steps.

|Actual Size (mm)|
| :---------: |
|![actual-size](assets/marker-actual-size.png)|

<br/><br/>
- - -
<br/><br/>

## **Optimizing Target Detection and Tracking**

To ensure the highest quality image target tracking experience, please follow these guidelines when designing your image targets.

**DO:**
* Rich in detail
* High contrast

**DON'T:**
* Repetitive patterns or motifs
* Too much bright or unclear in detail
* Low resolution images (glossy or blur)

**Color:** Color of image target does not effect much on tracking and detection. In order to get best results, use flat surfaces for image target tracking.<br>
However, consider the reflectivity of your image target's physical material. Glossy surface and screen reflections can lower tracking
quality. Use matter materials in diffuse lighting conditions for optimal tracking quality.

In sum up, these following properties and features of Image Target will enable the best detection and tracking performance for your AR apps.

|Properties|Examples|
|:---|:---|
|Rich in detail|Street or sport scenes, group  of people or mixtures of items|
|Good contrast|Bright and dark regions, and well-lit|
|No repetitive patterns|A grassy field, the facade of modern house with identical windows and a checkerboard|
|Format|Must be 8- or 24-bit PNG and JPG formats, less than 2MB in size|

<br/><br/>
For examples:

|GOOD Image Targets|BAD Image Targets|
|:---------: |:---------: |
|![street-scene-good](assets/road-good.png) | ![street-scene-bad](assets/road-bad.png)|
|![house-high-contrast](assets/house-good.png) | ![house-low-contrast](assets/house-bad.png)|
|![sport-good](assets/sport-good.png) | ![sport-bad](assets/sport-bad.png)|

<br/><br/>
One important thing, try to avoid repetitive patterns or motifs in order to get best results for tracking.<br>
These following images may contain enough features and good contrast, however repetitive patterns will affect the detection performance.<br/>
For best results, choose an image without repeated motifs (even if rotated and scaled) or strong rotational symmetry.
You might need to add more features such as texts, icons or customizing your image by changing the color and contrast
to make the image easier to distinguish.

|BAD Image Targets with Repetitive Patterns|GOOD Image Targets|
|:---------: | :---------: |
|<br/><br/>![bad](assets/ic-cancel-64.png)|<br/><br/>![good](assets/ic--ok-64.png)|
|![repeat-patterns](assets/repeat-patterns.png)|![opt-repeat-patterns](assets/opt-repeat-patterns.png)|
|These images might affect the detection and tracking for your AR apps since they have too much repetitive patterns. They look almost the same even when you rotate or scale.<br/><br/>|Try to avoid repetitive patterns by adding more features such as texts, customizing color, the contrast and so on.<br/><br/>|

<br/>
Also, you can also improve detection and tracking performance by controlling the focus mode of the device camera and designing
your app's user experience to obtain the best image of the target.

<br/><br/>
- - -
<br/><br/>

### **Lighting conditions**

The lighting conditions in your test environment might significantly affect target detection and tracking.

* Make sure that there is enough light in your room or operating workspace so that the scene details and target features
are well visible in the camera view.
* Too much brightness may produce bad results.
* Letsee WebAR SDK works best in indoor/outdoor environments, where the lighting conditions are usually more stable and easy to control.

|Lighting conditions|
|:---------: |
|![brightness](assets/brightness.png)|
|Make sure there is enough light in your working environment. For indoor/outdoor, the light<br>should be stable and easy to control (not too dark or too bright).|

<br/><br/>
- - -
<br/><br/>

### **Target Size**

* A physical printed image target can be **any size** you prefer as long as the markers are in camera's view, however you
should check its size carefully before setting up the Entity `.json` file for your project.

*Note:* Size of image target MUST be in `mm` for your Entity configuration.

|210 x 296 (mm)| 1000 x 560 (mm)|
|:---------: | :---------: |
|![escape-room-poster](assets/escape-room.png) | ![ultima-cena](assets/ultima_cena.png)||

<br/><br/>
- - -
<br/><br/>

### **Glossiness (printed image)**

Printed images from printers might also be glossy sometimes. Under certain viewing angles some light sources, such as a lamp, window,
or the sun, can create a glossy reflection that covers up large parts of the original texture of the printouts. The reflection
can create issues with tracking and detection, since this problem is very similar to partially occluding the target.

|Glossiness|
|:---------: |
|![glossy](assets/glossy.png)|

For good results and good AR experiences, you should take care not only lighting conditions but also viewing angle to ensure
the printed target is not too much glossy for detection.

<br/><br/>
- - -
<br/><br/>
