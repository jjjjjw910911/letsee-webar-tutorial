
This article guide you how to setup the Letsee SDK and APIs to build rich WebAR applications.

## **Setting Up**
To build AR content for your web page, you first need to include the Letsee WebAR SDK and set the AppKey and Tracker Type.
The following code shows the overall setup.

```html 
<!DOCTYPE html>
<html>
	<head>
		<script src="https://cdn.letsee.io/webar/Letsee.0.9.18.min.js/path/to/Letsee.0.9.9.js"></script>
	    <style media="place" type="text/css">
		    #container {
		        -letsee-target: uri('planar_entity.json');
		        -letsee-transform: translate(0, 0, 0);
		    }
	    </style>
    </head>
    <body>
        <div id="container">
            <!-- your AR contents -->
        </div>
    <script>
        const config = {
            "appKey": "your_app_key",
            "trackerType": "IMAGE"
        };
        const app = new Letsee(config);
    </script>
    </body>
</html>  
```

<br/><br/>
Letsee WebAR SDK is available via the CDN address below {Version description}:

```
https://cdn.letsee.io/webar/letsee-0.9.18.min.js
```

<br/><br/>
Letsee Engine has the following settings:

|Attributes|Required|Description|
|:---|:---|:---|
|`appKey`|Required|<br/>AppKey for license authorization.<br/><br/>|
|`trackType`|Required|<br/>Target recognition mode.<br/>You can set `IMAGE`, `MARKER` depends on what you want to track.<br/><br/>The IMAGE tracker recognizes and tracks one entity of the first declared `-letsee-target`. The MARKER tracker can track five markers simultaneously.<br/><br/>|

<br/>

Letsee Engine can be controlled by some of event commands such as `start`, `pause`, `resume`, and `stop`. Whenever the operational state changes,
the associated callback will be called. When the engine is started and activated, the viewport is forced to match the browser's viewport. So no matter what
device you are using and browser you are running, the viewport always fits to your device's UI.

Letsee 엔진은 start, pause, resume, stop 등의 명령으로 작동을 제어할 수 있습니다. 작동 상태가 변경될 때마다 관련된 콜백이 호출됩니다. 엔진이 활성화 되면 브라우저의 viewport가 카메라를 배경으로 한
AR의 UI에 대응하도록 viewport가 강제로 지정됩니다.

<br/><br/>
- - -
<br/><br/>

## **Recognition Targets Management**
### **Entity**
An Entity is an abstraction concept that Letsee proposes to contain the elements of the real world in augmented reality web apps.
The Entity is a physics object that exists in reality, such as objects, places and people. It has spatial attribute consisting of shapes,
size, features, position and so on.

All entities are web resources accessible via `HTTPS`. Therefore, Letsee WebAR SDK receives, tracks and reflects physically
identifiable data through the Uniform Resource Identifier (URI) which is declared within ARWebApp code. You can also declare the entity
yourself and add it into your web project if needed.

<br/>

|Properties|Required|Explanation|
|:---|:---|:---|
|`uri`|Required|<br/>A unique value that specifies the object. This `uri` is the same name as the file name.<br/><br/>|
|`name`|Optional|<br/>Name for the object.<br/><br/>|
|`size`|Required|<br/>Enter the actual size of the object. The unit is `mm`. <br/><ul><li>planner tracker : Actual size of the image target.</li><li>marker tracker: Actual size of the marker you wish to use.</li></ul>|
|`scale`|Optional|<br/>The scale of the content being augmented. Default is `1`. If the `"scale": 1` means the augmented content matches 1:1 with the size enter above.<br/><br/>|
|`image`|Required|<br/>For `IMAGE` Tracking.<br/><br/>Reference image to use for planar tracker. You can enter a URL or a relative path that begins with `https`.<br/>However, if you enter a relative path, be careful with the path of the web page where the AR app is running.</br><ul><li>Extension: Must be in **.jpg** or **.png** formats.</li><li>Capacity: Up to 2MB in size.</li><li>Pixel size: Up to 640,000 horizontal and vertical pixels.</li></ul> |
|`letseeMarkerId`|Required|<br/>For `MARKER` Tracking.<br/><br/>The marker ID to be used for tracking.<br/>A total of 250 markers are available, from 0 to 249 can be used. Input is `"marker_” + numeric`.<br/><br/>|

<br/><br/>

Here is a example of Entity configuration which is represented in JavaScript Object Notation (JSON) format:

```json
{
    "uri":"planar_entity.json",
    "name":"Sample entity",
    "size":{
        "width": 50,
        "height": 50
    },
    "scale": 1,
    "image":"/path/to/image.jpg"
}
```

<br/>

The Entity defined as above can be used within the AR WebApp code and will be subsequently created, loaded when Letsee Engine starts.
If a descriptor that matches the recognition method (`IMAGE`, `MARKER` or `QRCODE`), the entity is identified and correctly tracked within the camera.

<br/><br/>
- - -
<br/><br/>

## **HTML Elements and CSS Matching**
### **Place Media Type**

“Place” 미디어는 HTML 기반의 WebAR을 실현하기 위해 렛시가 제안하는 미디어 타입입니다. 미디어 타입은 HTML 문서를 렌더링 하기 위한 매체를 지정하기 위해 사용되며 대표적인 미디어 타입으로는 screen, print, speech 등이 있습니다. Letsee 엔진이 가동되면, Place 미디어로 선언된 CSS 구문들이 활성화 되어 HTML 문서를 다음과 같은 AR 모드로 렌더링 합니다.

* 브라우저의 배경화면이 카메라 영상으로 지정됩니다.
* ~~viewport 설정~~

### 인식 대상 설정하기

렛시 WebAR SDK는 인식을 위한 대상 물체를 지정하기 위한 -letsee-target css 속성을 소개합니다. 이 속성은 object (uri)를 사용합니다. 여기서 uri는 브라우저가 대상 객체를 나타내는 엔터티 데이터를 로드하는 URL 문자열입니다. 예를 들어 다음 구문은 CSS 셀렉터로 선택된 HTML 요소와 주어진 URL로 선언된 엔티티를 연관시킵니다. 따라서 URI로 지정한 엔터티가 브라우저의 뷰에 들어가면 선택된 HTML 요소가 정합되어 보여집니다.

```css
selector {
  -letsee-target: uri("https://d.letsee.io/fdkslhklvnsl");
}
```

타겟으로 지정되지 않은 요소는 일반 HTML 요소가 화면에 표시되는 것처럼 렌더링됩니다. 타겟이 지정된 HTML 요소가 화면에 보이지 않더라도 여전히 DOM에는 있습니다. 비디오 또는 오디오 요소가 자동 재생으로 설정되어 있으면 엔티티 추적 여부에 관계없이 리소스가 로드되는 즉시 재생됩니다. 화면에 표시될 때에만 재생하거나 시간 의존성이 있는 경우, 대상 엔터티에 대한 트래킹이 시작 또는 중지될 때 이벤트 핸들러를 함께 설정해 주십시오.

### HTML 요소 배치하기

렛시 WebAR SDK는 물리 객체 주위에 가상 객체 (HTML 요소)를 3D로 배치하기 위해 -letsee-transform css 속성을 소개합니다. 이 속성은 translate (x, y, z) 함수를 사용하여 요소의 위치를 픽셀 단위로 지정합니다. 엔터티 설명에서 언급했듯이 엔터티는 원통 또는 평면 모양입니다. 각 모양에 대한 좌표계는 다음과 같이 정의됩니다.

![이미지](http://drive.google.com/uc?export=view&id=1AUYzlrXpaaMvBFMHlGGSqJEG4CJVmAXJ) 
![이미지](http://drive.google.com/uc?export=view&id=1otjAdOLxgkchuWEyN5bfpqRxuL2-si1i)

y 축은 CSS 위치 속성의 방향과 반대입니다. 즉, 일반적인 CSS 스타일에서 일반적으로하는 것처럼 y 위치의 값을 높이는 대신 요소를 화면 아래로 (또는 대상 객체의 축 아래로) 이동하려면 값을 줄입니다.  
다음 예제를 보십시오.

```css
#hello {
  -letsee-target: uri("https://d.letsee.io/fdkslhklvnsl");
  -letsee-transform: translate(1, 2, 3);
}
```

위의 CSS는 id = hello인 HTML 요소를 x = 1, y = 2, z = 3에 배치합니다.

|속성|||설명|
|:---|:---|:---:|:---|
|-letsee-target|||entity 정보가 담긴 json 파일의 경로를 입력합니다. http나 https로 시작되는 url이나 상대 경로를 입력할 수 있습니다.<br/> 단, 상대 경로를 입력할 경우 AR App이 구동되는 웹 페이지의 경로에 주의해 주시기 바랍니다.|
|-letsee-transform|||인식된 화면상의 오브젝트의 중심점을 기준으로 어느 위치에 증강될 지를 결정합니다. 순서대로 x,y,z의 값을 px 기준으로 입력합니다.<br/>예를 들어 (-100, 100, 0)의 경우 오브젝트의 중심점을 기준으로 좌측으로 100px, 상단으로 100px 이동한 곳에 컨텐츠가 증강됩니다.<br/>css의 position: absolute와 유사하나, relative를 오브젝트의 센터에 두고 있다고 생각하시면 됩니다.|

```html
<head>
...
<style media="place" type="text/css">
#container {
    -letsee-target: uri('/path/to/entity.json');
    -letsee-transform: translate(0, 0, 0);
}
</style>
...
</head>
```

## JavaScript API를 이용한 동적 증강

'Renderable'은 렛시 브라우저가 엔터티를 확장하고 렌더링 할 수 있는 HTML 요소 또는 WebGL 3D 객체를 나타냅니다. 이 클래스는 DOM 트리의 HTML 요소와 동일합니다. DOM 트리와 마찬가지로 Renderable 객체는 장면 그래프를 형성합니다. 이러한 Renderable 객체의 위치와 스타일은 HTML 요소의 스타일을 지정하는데 사용되는 -letsee-transform 및 기타 CSS 속성과 동일합니다. 예를 들어,

```html
<style media="place">
#helloworld {
  -letsee-target: uri("https://d.letsee.io/some-eid");
}
</style>

<script>
  const app = new Letsee(config);
  app.onLoaded(function() {
  const entity = app.getEntity('https://d.letsee.io/some-eid');
  const helloworld = document.createElement('h1');
  helloWorld.appendChild(document.createTextNode('Hello World!'));
  const renderable = new DOMRenderable(helloWorld);
  renderable.position.set(100, 200, 300);
  entity.addRenderable(renderable);
});
</script>
```

DOMRenderable 클래스는 HTML 요소를 다루는 Renderable의 구현입니다.

Letsee.getEntity는 엔터티 객체를 반환합니다. 그런 다음 HTML 요소 객체를 가져와서 대부분의 OpenGL 구현에서 3D 객체와 동일한 DOMRenderable 인스턴스를 만들 수 있습니다. 엔터티 객체에서 addRenderable을 호출하여 이 객체를 엔터티에 연관시키도록 지정합니다.

## 이벤트

|이벤트|설명|
|:---|:---|
|onTrackStart|엔터티가 뷰에 들어가면 Letsee 엔진의 이벤트 관리자가 trackstart 유형의 이벤트를 전달합니다.|
|onTrackMove|엔터티가 뷰 안에서 이동하면 Letsee 엔진의 이벤트 관리자가 trackmove 타입의 이벤트를 전달합니다.<br/> trackmove 이벤트의 경우 이벤트 객체에는 장치 카메라를 기준으로 엔티티의 로컬 3D 변환 매트릭스가 포함됩니다.|
|onTrackEnd|엔터티가 뷰를 벗어나면 Letsee 엔진의 이벤트 관리자가 trackend 타입의 이벤트를 전달합니다. |

## WebGL로 3D 모델 정합하기

Letsee Web AR SDK과 three.js를 이용하여 3D 오브젝트를 증강할 수 있습니다. three.js의 공식 example중 obj 파일을 로드하는 샘플 

[https://threejs.org/examples/?q=obj#webgl_loader_obj](https://threejs.org/examples/?q=obj#webgl_loader_obj) 

을 Letsee Web AR SDK를 이용하여 제작해 보겠습니다.  
(해당 리포에 있는 커스텀 로더 및 오브젝트 파일, 텍스쳐를 미리 준비할 필요가 있음)

three.js의 예제에서는 크게

1. THREE.PerspectiveCamera, THREE.Scene, THREE.WebGLRenderer를 생성
2. 3D 오브젝트 로드
3. 3D 오브젝트를 THREE.Scene에 add
4. THREE.WebGLRenderer에서 THREE.PerspectiveCamera, THREE.Scene를 렌더
5. 4)를 프레임마다 반복

의 과정을 거쳐 화면상에 3D오브젝트를 렌더링 합니다.  
Letsee Web AR SDK에서는

1. Letsee 객체에서 camera, scene, renderer를 취득
2. 3D 오브젝트 로드
3. 3D 오브젝트를 entity에 add

의 과정을 거치에 되며 Entity에 등록된 오브젝트는 해당 엔터티가 화면상에 트랙킹이 되면 자동으로 렌더링됩니다.

```js
// Letsee 객체 생성
const app = new Letsee(config);

// three js 렌더러와 씬, 카메라를 생성한다.
// Letsee의 증강 환경과 싱크를 맞추기위해 renderer, scene, camera를 Letsee에서 취득해 온다.
let threeRenderer, scene, camera;

// 증강할 3D 오브젝트를 생성한다.
let object;
app.onLoaded(function() {
    // three.js에서 사용할 webgl renderer, scene, camera를 Letsee에서 받아온다.
    threeRenderer = app.threeRenderer;
    scene = threeRenderer.scene;
    camera = threeRenderer.camera;

    // three.js 어플리케이션을 시작한다. rendering은 Letsee의 증강 사이클에 맞추어 자동으로 동작하게 된다.
    init();
});


// 3D를 로딩후 Entity에 연결

function init() {
    const ambientLight = new THREE.AmbientLight( 0xcccccc, 0.4 );
    scene.add( ambientLight );
    const pointLight = new THREE.PointLight( 0xffffff, 0.8 );
    camera.add( pointLight );
    scene.add( camera );

    function loadModel() {
      object.traverse( function ( child ) {
        if ( child.isMesh ) child.material.map = texture;
      } );
      object.position.y = - 95;

      // 증강할 오브젝트를 트랙킹할 Entity와 함께 renderer에 등록한다.
      threeRenderer.addObjectToEntity('toystory.json', object);
    }

    const manager = new THREE.LoadingManager( loadModel );
    manager.onProgress = function ( item, loaded, total ) {
      console.log( item, loaded, total );
    };

    // texture

    const textureLoader = new THREE.TextureLoader(manager);
    const texture = textureLoader.load( 'textures/UV_Grid_Sm.jpg' );

    // model
    function onProgress( xhr ) {
      if ( xhr.lengthComputable ) {
        const percentComplete = xhr.loaded / xhr.total * 100;
        console.log( 'model ' + Math.round( percentComplete, 2 ) + '% downloaded' );
      }
    }

    function onError() {}

    const loader = new THREE.OBJLoader( manager );

    loader.load( 'models/obj/male02/male02.obj', function ( obj ) {
      object = obj;
    }, onProgress, onError );
  }

```
