# Hello, world!
Letsee WebAR SDK를 이용한 Hello, world! 입니다.

---

## Explain
### SDK 임포트
Web AR 어플리케이션을 만들기 위한 Letsee Web AR SDK를 임포트 합니다.
```html
<script src="https://app.letsee.io/webar?key=YOUR_APP_KEY"></script>
```
### 증강을 위한 Entity 설정
Letsee Web AR SDK는 증강할 대상과 증강될 컨텐츠를 css를 통해 설정합니다. `media="place"`태그를 사용한 css를 통해 손쉽게 설정할 수 있습니다.  
`letsee-marker.json`파일에는 증강할 대상의 정보가 담겨 있습니다.
```html
<style media="place" type="text/css">
  #container {
    -letsee-target: uri('letsee-marker.json');
  }
</style>
```

#### letsee-marker.json 내용
```json
{
	"uri":"letsee-marker.json",
	"name":"Letsee Web AR SDK",
	"size":{
 		"width": 180,
		"height": 180
	},
	"scale": 1,
	"image":"custom_marker.jpg"
}
```

### 증강할 오브젝트 생성
Letsee Web AR SDK는 일반적인 HTML Element를 증강 합니다.
```html
<div id="container"><h2 style="color: white">Hello, world!</h1></div>
```

### 어플리케이션 실행
어플리케이션 구동을 위한 appKey와 증강 타입을 지정해서 Letsee 객체를 생성하게 되면 Web AR 어플리케이션 제작이 완료됩니다.
```js
const config = {
  "appKey": "your_app_key",
  "trackerType": "IMAGE",
};
const letsee = new Letsee(config);
```

---

## What you should see
<br/><br/>
<center><img src="assets/screenshots/helloworld_screenshot.jpeg" width="360" style="border:1px solid gray; border-radius: 5px"/></center>
<br/><br/>

---

## Final code
```html
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Letsee WebAR Demo</title>
<script src="https://cdn.letsee.io/webar/letsee-0.9.18.min.js"></script>
<style media="place" type="text/css">
  #container {
    -letsee-target: uri('letsee-marker.json');
  }
</style>
</head>
<body>
<div id="container"><h1>Hello, world!</h1></div>
<script>
  const config = {
    "appKey": "your_app_key",
    "trackerType": "IMAGE",
  };
  const letsee = new Letsee(config);
</script>
</body>
</html>
```
