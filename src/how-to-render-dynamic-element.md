
# JavaScript API를 이용한 동적 증강

`Renderable`은 렛시 브라우저가 엔터티를 확장하고 렌더링 할 수 있는 HTML 요소 또는 WebGL 3D 객체를 나타냅니다. 이 클래스는 DOM 트리의 HTML 요소와 동일합니다. DOM 트리와 마찬가지로 `Renderable` 객체는 장면 그래프를 형성합니다.  
이러한 Renderable 객체의 위치와 스타일은 HTML 요소의 스타일을 지정하는데 사용되는 `-letsee-transform` 및 기타 CSS 속성과 동일합니다. 아래의 예제를 통해 동적으로 renderable을 생성하고 증강하는 과정을 살펴보겠습니다.

---

## 준비사항
### Letsee Web AR SDK
Web AR 어플리케이션에서 사용할 마커 이미지와 엔터티 설정 파일이 필요합니다. 아래 예제에서 사용한 마커와 엔터티 파일은 Letsee에서 제공하는 샘플 입니다.
- [Download Letsee marker image](https://github.com/letsee/tutorials/blob/master/sample_entity/custom_marker.jpg) 
- [Download Letsee marker json](https://github.com/letsee/tutorials/blob/master/sample_entity/letsee-marker.json) 

Letsee Web AR SDK를 사용하기 위해서는 SDK파일과 appKey가 필요합니다. SDK 파일은 cdn을 통해 제공됩니다.  
[인증을 위한 appKey는 이곳을 확인해 주시기 바랍니다.](/getting-started?id=인증키)

- CDN 주소: `https://cdn.letsee.io/webar/letsee-0.9.18.min.js`


## 코드 설명
### Entity 설정
증강을 위한 엔터티를 생성해 줍니다. html 바디안에 `id='helloworld'`인 html코드는 생성하지 않습니다.

```html
<style media="place">
  #helloworld {
    -letsee-target: uri('letsee-marker.json');
  }
</style>
```

### Letsee 시작
```js
const config = {
  "appKey": "your_app_key",
  "trackerType": "IMAGE",
};
const letsee = new Letsee(config);
```

### onLoad 이벤트
동적으로 renderable 객체를 만들기 위해서는 Letsee가 완전히 로드된 후에 사용을 해야 합니다. `onLoad`를 이용하여 Letsee가 완전히 기동된 후, 증강 대상을 생성합니다.
```js
letsee.onLoad(function() {
  // Do something
});
```

### Renderable 생성 및 Entity에 연결
`onLoad`이벤트의 콜백으로 Renderable을 생성합니다. 순서는 크게
1. HTML Element 생성
2. 생성된 HTML Element를 이용하여 Renderable 생성
3. Entity 취득
4. Entity에 Renderable 연결 입니다. 

```js
// HTML Element를 생성하고 text를 추가
const helloworld = document.createElement('h1');
helloworld.appendChild(document.createTextNode('Hello World!'));

// Renderable 생성
const renderable = new DOMRenderable(helloworld);

// css를 통해 생성한 Entity 객체를 취득
const entity = letsee.getEntity('letsee-marker.json');
// Entity에 Renderable을 등록
entity.addRenderable(renderable);
```

### 위치 지정
필요하다면 css의 `-letsee-transform`과 동일한 개념으로 증강 객체의 위치를 지정할 수 있습니다. `Renderable.position.set(x, y, z)`을 이용합니다.  
position은 언제든지 업데이트할 수 있습니다. 아래의 결과는 오른쪽으로 100px, 위로 200px을 이동시킵니다.
```js
renderable.position.set(100, 200, 0);
```

---------

## What you should see

<br/><br/>
<center><img src="assets/screenshots/helloworld_screenshot.jpeg" width="360" style="border:1px solid gray; border-radius: 5px"/></center>
<br/><br/>


---

## Fullcode
[소스 코드는 이곳에서 다운받을 수 있습니다.](https://github.com/letsee/tutorials/tree/master/dynamic-dom-element-sample)

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Letsee WebAR Demo</title>
  <script src="https://cdn.letsee.io/webar/letsee-0.9.18.min.js"></script>
  <style media="place">
    #helloworld {
      -letsee-target: uri('letsee-marker.json');
    }
  </style>
</head>
<body>
<script>
  const config = {
    "appKey": "your_app_key",
    "trackerType": "IMAGE",
  };
  const letsee = new Letsee(config);
  letsee.onLoad(function() {
    const helloworld = document.createElement('h1');
    helloworld.appendChild(document.createTextNode('Hello World!'));
    const renderable = new DOMRenderable(helloworld);
    const entity = letsee.getEntity('letsee-marker.json');
    entity.addRenderable(renderable);
  });
</script>
</body>
</html>
```
