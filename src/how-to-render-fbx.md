# How to render FBX
Letsee WebAR SDK는 다양한 WebGL 기반의 3D 프레임워크를 지원합니다. 
이 튜토리얼은 Three.js를 이용한 3D 기반의 ARWebApp을 만드는 방법을 설명합니다.

---------

## 준비사항

### three.js
아래의 예제는 three.js의 [FBX Loader 예제](https://threejs.org/examples/?q=fbx#webgl_loader_fbx)를 Letsee Web AR SDK에 맞추어 변환한 것 입니다.  
3D파일을 로드하기 위한 스크립트, 3D 오브젝트 파일등은 [three.js의 github](https://github.com/mrdoob/three.js/tree/r105/examples)에서 다운로드 받을 수 있습니다.
 
- [Download FBXLoader.js](https://github.com/mrdoob/three.js/blob/r105/examples/js/loaders/FBXLoader.js)
- [Download inflate.min.js](https://github.com/mrdoob/three.js/blob/r105/examples/js/libs/inflate.min.js)
- [Download FBX Object file](https://github.com/mrdoob/three.js/blob/r105/examples/models/fbx/Samba%20Dancing.fbx)
> three.js는 r105 버전을 기준으로 설명합니다.

### Letsee Web AR SDK
Web AR 어플리케이션에서 사용할 마커 이미지와 엔터티 설정 파일이 필요합니다. 아래 예제에서 사용한 마커와 엔터티 파일은 Letsee에서 제공하는 샘플 입니다.
- [Download Letsee marker image](https://github.com/letsee/tutorials/blob/master/sample_entity/custom_marker.jpg) 
- [Download Letsee marker json](https://github.com/letsee/tutorials/blob/master/sample_entity/letsee-marker.json) 

Letsee Web AR SDK를 사용하기 위해서는 SDK파일과 appKey가 필요합니다. SDK 파일은 cdn을 통해 제공됩니다.  
[인증을 위한 appKey는 이곳을 확인해 주시기 바랍니다.](/getting-started?id=인증키)

- CDN 주소: `https://cdn.letsee.io/webar/letsee-0.9.18.min.js`


---------

## 코드 설명
### js 임포트
Letsee Web AR SDK와 three.js를 위한 javascript를 임포트 합니다.  
three.js의 [FBX loading sample](https://github.com/mrdoob/three.js/blob/r105/examples/webgl_loader_fbx.html)를 참고하여 [FBXLoader.js](https://github.com/mrdoob/three.js/blob/r105/examples/js/loaders/FBXLoader.js)와 [inflate.min.js](https://github.com/mrdoob/three.js/blob/r105/examples/js/libs/inflate.min.js)를 준비합니다.

```html
<script  src="https://cdn.letsee.io/webar/letsee-0.9.18.min.js"></script>
<script  src="https://cdnjs.cloudflare.com/ajax/libs/three.js/105/three.min.js"></script>
<script  src="js/loaders/FBXLoader.js"></script>
<script  src="js/libs/inflate.min.js"></script>
```
### 증강을 위한 Entity 설정
`head`태그 내부에 설정된 `media="place"`으로 선언된 css를 이용하여 증강 대상을 지정합니다.
```html
<style  media="place"  type="text/css">
	#container {
		-letsee-target: uri('letsee-marker.json');
	}
</style>
```

### Web AR SDK 실행
appKey와 trackerType을 설정하여 어플리케이션을 실행시킵니다. onLoad이벤트를 통해 어플리케이션이 실행된 다음 three.js를 위한 함수를 실행시킵니다.
```js
const config = {
	"appKey": "your_app_key",
	"trackerType": "IMAGE"
};
const letsee = new Letsee(config);
letsee.onLoad(() => init());
```

### three.js 코드
[FBX loading sample](https://github.com/mrdoob/three.js/blob/r105/examples/webgl_loader_fbx.html)을 바탕으로한 3d 오브젝트 증강 예제 입니다.

#### Mixer 생성
애니메이션을 위한 mixer를 생성합니다.
```js
let mixer;
```

#### three.js 오브젝트 생성
three.js에서 사용할 renderer, camera, scene를 Letsee 객체에서 생성합니다. Letsee의 증강 데이터와 싱크를 맞추기 위해서는 반드시 Letsee객체를 이용하여야 합니다.
```js
const renderer = letsee.threeRenderer;
const camera = renderer.camera;
const scene = renderer.scene;
```

#### 라이트 생성
렌더링을 위한 라이트를 생성합니다.
```js
const light = new THREE.DirectionalLight(0xffffff);
light.position.set(0, -95, 120);
scene.add(light);
```

#### 3D 오브젝트 로드
FBX Loader를 사용하여 3D 오브젝트를 로드하고 증강대상과의 정합을 위해 entity와 함께 Letsee의 threeRenderer에 등록합니다.
```js
const loader = new THREE.FBXLoader();
loader.load('models/fbx/Samba Dancing.fbx', function (object) {
	mixer = new THREE.AnimationMixer(object);
	const action = mixer.clipAction(object.animations[0]);
	action.play();
	object.position.y = - 95;
	renderer.addObjectToEntity('letsee-marker.json', object)
});
```

#### 커스텀 애니메이션 설정
프레임 단위로 실행되어야할 커스텀 함수를 등록합니다. addRenderModule을 통해 등록된 콜백 함수는 **ms단위**의 tick을 받아옵니다.  addRenderModule은 Letsee.onLoad에 기술되어야 합니다.
```js
let oldTime = null;
const animate = (tick) => {
	if (oldTime ) {
		const delta = tick - oldTime;
		if (mixer) mixer.update(delta/1000);
	}
	oldTime = tick;
}
letsee.addRenderModule({
	name: 'fbxAnimation',
	onRender: animate
});	
```

---------

## What you should see
<br/><br/>
<center><img src="assets/screenshots/fbx_loader_screenshot.PNG" width="360" style="border:1px solid gray; border-radius: 5px"/></center>
<br/><br/>

---------

## Final code

[소스 코드는 이곳에서 다운받을 수 있습니다.](https://github.com/letsee/tutorials/tree/master/ar-fbx-animation-sample)

```html
<!DOCTYPE html>
<html>
<head>
// Letsee Web AR SDK 임포트
<script src="https://cdn.letsee.io/webar/letsee-0.9.18.min.js"></script>
// three.js 임포트
<script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/105/three.min.js"></script>
// FBX 로더 관련 파일 임포트
<script src="js/loaders/FBXLoader.js"></script>
<script src="js/libs/inflate.min.js"></script>
// 엔터티 설정
<style  media="place" type="text/css">
  #container {
    -letsee-target: uri('letsee-marker.json');
  }
</style>
</head>
<body>
<script>
  // 어플리케이션 설정
  const config = {
    "appKey": "your_app_key",
    "trackerType": "IMAGE"
  };
  // 어플리케이션 시작
  const letsee = new Letsee(config);
  // 로드 이벤트 취득 후 three.js 실행
  letsee.onLoad(() => init());
  // 애니메이션을 위한 믹서 설정
  let mixer;
  function init() {
    // three.js를 위한 렌더러, 카메라, 신 설정
    const renderer = letsee.threeRenderer;
    const camera = renderer.camera;
    const scene = renderer.scene;
    // 라이팅 설정
    let light;
    light = new THREE.DirectionalLight(0xffffff);
    light.position.set(0, -95, 120);
    scene.add(light);
    // FBX 파일 로드
    const loader = new THREE.FBXLoader();
    loader.load('models/fbx/Samba Dancing.fbx', function (object) {
      // FBX 애니메이션 설정
      mixer = new THREE.AnimationMixer(object);
      const action = mixer.clipAction(object.animations[0]);
      action.play();
      // 증강 대상의 중심에 오도록 위치 수정
      object.position.y = - 95;
      // Letsee에 3D 오브젝트 등록
      renderer.addObjectToEntity('letsee-marker.json', object)
    });
    // 애니메이션을 위한 함수를 Letsee에 등록
    letsee.addRenderModule({
      name: 'fbxAnimation',
      onRender: animate
    });	
  };
  // 애니메이션을 위한 함수
  let oldTime = null;
  const animate = (tick) => {
    if (oldTime ) {
      const delta = tick - oldTime;
      if (mixer) mixer.update(delta/1000);
    }
    oldTime = tick;
  }
</script>
</body>
</html>
```

