# Letsee Web AR SDK Tutorials

Docsify로 제작하였음. 자세한 사항은 [Docsify](https://docsify.js.org/)를 참고!!


## 버전 관리
외부 공개 문서이니 버전 관리는 조심히 하자. (궁서체라 생각하며 읽어야 함.....)

업데이트/수정이 이루어지게 되면
1. 수정후 커밋 없이 로컬 확인
2. 로컬 확인 후 dev 확인(intra 서버가 될수도 있고, 자신의 로컬을 공유할수도 있고)
3. 컨펌 후 커밋
4. 커밋 내용을 다음 버전에 수정 이력으로 추가.

업데이트/수정이 반복된다면 위의 프로세스를 반복하자. 습관적으로 수정->커밋을 반복하면 자잘한 커밋이 너무 많아진다.  

상기 프로세스를 거쳐 릴리즈를 할때는
1. 모든 수정 컨펌 커밋 후 재확인
2. 재확인이 완료되면 `git tag X.X`로 태깅.
3. X.X 버전은 아래의 버전 이력을 확인하여 올라간다.
4. `git push origin X.X`로 태그를 푸시.
5. 디플로이시에는 `git pull origin X.X`로 소스 업데이트(당연히 라이브 서버에서)
6. 아래 current version에 변경 이력을 남긴다.

어지간한 소스를 제외하고 라이브 디플로이는 대크 기반이 기본이다.  

그중 tutorial과 같은 도큐먼트 베이스의 프로젝트에서 tag가 중요한 이유는 `SDK 버전에 따른 문서 관리`를 위해서이다.  

가령 1.0의 튜토리얼은 SDK 0.9.18을 위한 더큐먼트가 될것이고, 0.9.18에 대한 업데이트는 1.1, 1.2, 1.3등으로 올라갈것이다.  
그런데 만약 SDK가 매이저 업데이트 되어 API가 바뀐다면??? 튜토리얼 내의 모든 소스 코드를 바꾸고 2.x대로 같이 메이저 업데이트를 해야 한다.  
그러면 배포는 뭘 해야 할까? 문서를 이력이 남아야 하기때문에 기본의 모든 버전을 다 공개햐야 한다. SDK가 1로 올라가도 0.9를 안쓴다는 보장이 없어서 그렇다.  
docsify에도 있을것으로 생각되는데 도큐멘테이션 솔루션은 대부분 태그 기반으로 버저닝을 바꿔주는 기능이 있어서 도큐먼트 태깅은 꼭꼭꼭 해줘야 한다.


## How to deploy to server

### 현재 사용하고 있는 서버 접속
`ssh -i ~/aws_rsa.pem ubuntu@ec2-52-79-101-226.ap-northeast-2.compute.amazonaws.com`  
**현재 사용하고 있는 ec2인스턴스이고 위의 주소는 언제던지 바뀔 수 있다.**  

- repo root: `/home/ubuntu/www/tutorial_repo`
- web root: `/home/ubuntu/www/developers.letsee.io/public_html`
- tutorials root: `/home/ubuntu/www/developers.letsee.io/public_html/tutorials`

디플로이는 `repo root`로 이동하여 `git fetch -tag`로 최신 태그를 모두 불러오고 디플로이할 tag로 checkout 해주면 된다.  
`git checkout X.X`  

`/home/ubuntu/www/tutorial_repo/src`가 `/home/ubuntu/www/developers.letsee.io/public_html/tutorials`로 심링크 걸려있다.


**주의사항**
1. web root와 tutorials root는 다르다.
2. 일반적인 상황에서는 repo root만 업데이트 하면 된다.


### current version
- **1.0**: release at 2019.09.27
    - First release
    - For SDK 0.9.18
 
- **1.1**: release at 2019.10.01
    - Fix typo
    - Fix entity size: 800 to 200. Use real unit with mm

- **1.2**: release at --------
    - Fix typo
      - HTML file annotation change. (image width, height have to be enrolled in mm unit)
      - Json file format explanation update. (mm: pixel size, marker_001 [x])  
    - Change marker image to new one. / Fix typo about json string and link address
     
## How to check at local?

Docsify는 index.html만 불러 들이면 내장 라이브러리가 md파일을 파싱하는 구조라 그 어떤 방법을 사용해서 web server만 띄우면 된다.  
이도 저도 귀찮으면 docsify-cli를 이용하면 된다.
`docsify serve --open ./src`
or
`npm run docsify`
docsify-cli로 서버를 올리면 리도드 하지 않더라도 수정 되는 사항을 실시간으로 업데이트 해준다.

## How to deploy
위에 언급하였듯이 static하게 index.html만 호출하면 되기때문에 그냥 apache나 nginx의 스태틱 서빙이면 충분하다.

## Markdown
Docsify에서 사용하는 몇가지 문법이 있는듯하여 [이곳의 링크를 건다](https://github.com/jhildenbiddle/docsify-themeable/blob/master/docs/markdown.md)
(사실 Markdown의 문법은 몇개 되지 않지만, 보통은 md->html로 변형을 많이 하다보니 Markdown parser에 따라 추가/변형 문법이 많은것뿐.)


## TODO
- 다국어 지원. `https://docsify.js.org/#/custom-navbar`를 이용하면 된다.
- 버전 지원.  `https://docsify.js.org/#/custom-navbar`를 이용하면 된다.


## 폴더 구도 예상도
상상도. 사이드바와 나브바 사용법등에 대해 테스트가 필요함.  
중요한 포인트는 
- 언어/버전별로 조합하는 구조
- 기본버전을 설정하여 보여주는것
- 버전별 폴더는 각각의 별도 git으로 관리되어야 하는데 동일한 리포를 서브깃으로 관리할 수 없다는 함정
- 파일 기반의 배포방식이 이상적일수도 있는데 그러려면 jenkins같은 CI툴을 써야한다는 함정
이다.

*버전별 폴더 관리*
```
+-- src
    +-- _coverpage.md
    +-- _sidebar.md
    +-- 0.9.18
        +-- en_US
            +-- _sidebar.md
            +-- getting-started.md
            +-- .......
        +-- ko_KR
            +-- _sidebar.md
            +-- getting-started.md
            +-- .......
    +-- 1.0
        +-- en_US
            +-- _sidebar.md
            +-- getting-started.md
            +-- .......
        +-- ko_KR
            +-- _sidebar.md
            +-- getting-started.md
            +-- .......
```

*과거 버전만 별도 포러 관리*
```
+-- src
    +-- _coverpage.md
    +-- _sidebar.md
    +-- en_US
        +-- _sidebar.md
        +-- getting-started.md
        +-- .......
    +-- ko_KR
        +-- _sidebar.md
        +-- getting-started.md
        +-- .......
    +-- 0.9.18
        +-- en_US
            +-- _sidebar.md
            +-- getting-started.md
            +-- .......
        +-- ko_KR
            +-- _sidebar.md
            +-- getting-started.md
            +-- .......
```
